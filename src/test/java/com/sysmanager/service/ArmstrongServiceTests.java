package com.sysmanager.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Testes do serviço que valida números Armstrong
 * 
 * @author Marcio Cruz de Almeida
 *
 */
@SpringBootTest
public class ArmstrongServiceTests {

	private static final int ARMSTRONG_VALID_3_DIGITS = 153;
	private static final int ARMSTRONG_INVALID_3_DIGITS = 150;
	
	private static final int ARMSTRONG_VALID_4_DIGITS = 1634;
	private static final int ARMSTRONG_INVALID_4_DIGITS = 1000;
	
	@Autowired
	private ArmstrongService armstrongService;

	/**
	 * Testa números armstrong de 3 dígitos validos.
	 */
	@Test
	void armstrongValid3DigitsTest() {
		assertTrue(armstrongService.armstrongNDigits(ARMSTRONG_VALID_3_DIGITS));
	}
	
	/**
	 * Testa números armstrong de 3 dígitos invalidos.
	 */
	@Test
	void armstrongInvalid3DigitsTest() {
		assertFalse(armstrongService.armstrongNDigits(ARMSTRONG_INVALID_3_DIGITS));
	}
	
	/**
	 * Testa números Armstrong de N dígitos validos. 
	 * Neste caso está testando um número de 4 dígitos.
	 */
	@Test
	void armstrongValidNDigitsTest() {
		assertTrue(armstrongService.armstrongNDigits(ARMSTRONG_VALID_4_DIGITS));
	} 
	
	/**
	 * Testa números Armstrong de N dígitos validos. 
	 * Neste caso está testando um número de 4 dígitos.
	 */
	@Test
	void armstrongInvalidNDigitsTest() {
		assertFalse(armstrongService.armstrongNDigits(ARMSTRONG_INVALID_4_DIGITS));
	}
}
