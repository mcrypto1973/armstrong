package com.sysmanager.job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.sysmanager.service.ArmstrongService;

@Component
public class ArmstrongJob implements ApplicationRunner {

	@Autowired
	private ArmstrongService armstrongService;
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		
		String word = "";
		for (String arg : args.getSourceArgs()) {
			word = arg;
		}
		
		try {
			int num = Integer.parseInt(word);
			
			if (armstrongService.armstrongNDigits(num)) {
				System.out.println("É um número Armstrong");
			}
			else {
				System.out.println("Não é um número Armstrong");
			}
		}
		catch (NumberFormatException ex) {
			System.out.println("Somente números são permitidos");
		}
		
	}

}
