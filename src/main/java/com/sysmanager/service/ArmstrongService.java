package com.sysmanager.service;

import org.springframework.stereotype.Service;

/**
 * Serviço que valida um número Armstrong
 * 
 * @author Marcio Cruz de Almeida
 *
 */
@Service
public class ArmstrongService {

	/**
	 * Valida se o número de N dígitos passado é um número Armstrong
	 * @param num 	um número de N digitos
	 * @return		<code>true</code> se é um número Armstrong valido<br>
	 * 				<code>false</code> se é um número Armstrong invalido
	 */
	public boolean armstrongNDigits(int num) {
		int temp = num;
		int remainder;
		int result = 0;
		int n = 0;
		
		while (temp != 0) {
			temp /= 10;
			n++;
		}
		
		temp = num;
		while (temp != 0) {
			remainder = temp % 10;
			result += Math.pow(remainder, n);
			temp /= 10;
		}
		
		if (result == num) {
			return true;
		}
		return false;
	}
}
