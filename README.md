# ARMSTRONG

## Observações

- Eu desenvolvi no Spring Tool Suite
- Estou executando com o comando
  - spring-boot:run -DskipTests -Dspring-boot.run.arguments=153
- Os testes estão abrangendo somente o serviço que trata os n[umeros, são apenas teste unitários
- Tudo foi enviado direto para o master, não utilizei o git flow nem me importei com issues (não é assim que trabalho, mas como era apenas um teste não levei em consideração este rito)
  
<span style="color:red">*Geralmente não se faz Java Docs sobre funções privadas ou protegidas, mas para um melhor entendimento do avaliador eu fiz*</span>

## Questão 3
<p>Escreva uma função que obtenha um inteiro como entrada e calcule se tal inteiro é um número Armstrog.</p>

<p>Dica: Um número Armstrog é aquele cuja dos cubos de cada dígito do número é igual ao próprio número.</p>

Exemplo:

- 153 = (1*1*1) + (5*5*5) + (3*3*3)
